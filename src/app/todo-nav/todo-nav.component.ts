import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo-nav',
  templateUrl: './todo-nav.component.html',
  styleUrls: ['./todo-nav.component.scss']
})
export class TodoNavComponent implements OnInit {

  appTitle : string;

  constructor() {
      this.appTitle = 'ToDo App (v1.0)';
  }

  ngOnInit() {
      // indítok egy ajax lekérdezést
  }

}
