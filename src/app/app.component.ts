import { Component, ElementRef, ViewChild, ApplicationRef, OnInit } from '@angular/core';
import { trigger, transition, animate, style, query } from '@angular/animations';

import { Todo, Project } from './interfaces';
import { LocalstorageService } from './localstorage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
      trigger('preventInitialChildAnimations', [
        transition(':enter', [
          query(':enter', [], {optional: true})
        ])
      ]),
      trigger('slideInOut', [
        transition(':enter', [
          style({transform: 'translateX(-50%)', opacity: 0}),
          animate('200ms ease-out', style({transform: 'translateX(0%)', opacity : 1}))
        ]),
        transition(':leave', [
          animate('200ms ease-in', style({transform: 'translateX(50%)', opacity: 0}))
        ])
      ])
    ]
})
export class AppComponent implements OnInit {
  
    projects : Project[];
    activeProject : Project;
    createProject : boolean;
    createProjectName : string;
    createItem : boolean;
    createItemName : string;

    @ViewChild('projectNameInput')
    projectNameInput : ElementRef;

    constructor (private appref : ApplicationRef, private lss : LocalstorageService) {
        /*
        this.projects = [
            { name : "Proj 1", items : [] },
            { 
                name : "Proj 2",
                items : [
                    {
                        name : "Design space rocket",
                        isComplete : true,
                        date : { year : 2018, month : 7, day: 1 }
                    },
                    {
                        name : "Build space rocket",
                        isComplete : false,
                        date : { year : 2018, month : 8, day: 1 }
                    },
                    {
                        name : "Launch space rocket",
                        isComplete : false,
                        date : { year : 2018, month : 9, day: 1 }
                    }
                ]
            }
        ];
        */
        this.projects = [];
        this.activeProject = this.projects[0];
        this.createProject = false;
        this.createProjectName = '';
        this.createItem = false;
        this.createItemName = '';
    }

    ngOnInit() : void {
        this.lss.loadProjects().then( projs => {
            this.projects = projs;
        } );
    }

    saveState() : void {
        this.lss.saveProjects( this.projects );
    }

    setActiveProject( p : Project, e? : any ) : void {
        this.activeProject = p;
        this.setItemCreateMode(false);
        if (e) {
            e.preventDefault();
        }
        this.saveState();
    }

    setCreateMode( mode : boolean, e? : any ) : void {
        this.createProject = mode;
        if (e) {
            e.preventDefault();
        }
        if (mode) {
            this.appref.tick();
            if (this.projectNameInput) {
                this.projectNameInput.nativeElement.focus();
            }
        }
    }

    setItemCreateMode( mode : boolean, e? : any ) : void {
        this.createItem = mode;
        if (e) {
            e.preventDefault();
        }
        if (mode) {
            this.appref.tick();
            // this.projectNameInput.nativeElement.focus();
        }
    }

    saveProject( e : any ) : void {
        if (e.which == 13) {
            this.setCreateMode(false);
            if (this.createProjectName != '') {
                this.projects.push( { name : this.createProjectName, items : [] } );
                this.createProjectName = '';
                this.saveState();
            }
        }
    }

    saveItem( e : any ) : void {
        if (e.which == 13) {
            this.setItemCreateMode(false);
            if (this.createItemName != '') {
                let d = new Date();
                this.activeProject.items.push(
                    {
                        name: this.createItemName,
                        isComplete: false,
                        date: { year: d.getFullYear(), month: d.getMonth()+1, day: d.getDate() }
                    }
                );
                this.createItemName = '';
                this.saveState();
            }
        }
    }

    deleteActiveProjectItem( item: Todo ) : void {
        this.activeProject.items = this.activeProject.items.filter( i => {
            return item != i;
        } );
    }

}
