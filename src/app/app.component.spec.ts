import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { TodoNavComponent } from './todo-nav/todo-nav.component';
import { TodoItemComponent } from './todo-item/todo-item.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        TodoNavComponent,
        TodoItemComponent
      ],
      imports: [
        FormsModule,
        NgbModule.forRoot()
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`createProject should be false on init`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.createProject).toBeFalsy();
  }));
  it('after setCreateMode(true) createProject should be true', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    app.setCreateMode(true);
    expect(app.createProject).toBeTruthy();
  }));
});