import { NgbDateStruct } from '../../node_modules/@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-struct';

export interface Todo {
    name : string;
    isComplete : boolean;
    date : NgbDateStruct;
}
export interface Project {
    name : string;
    items : Todo[];
}