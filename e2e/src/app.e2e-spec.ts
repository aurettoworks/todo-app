import { promise, Key } from 'protractor';

import { AppPage } from './app.po';

describe('App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should be branded', () => {
    page.navigateTo();
    expect(page.getNavbarText()).toEqual('ToDo App (v1.0)');
  });
  it('should be able to add project', () => {
      page.navigateTo();
      page.getAddProjectLink().click();
      expect(page.getAddProjectInput().isPresent()).toBeTruthy();
      expect(page.getAddProjectLink().isPresent()).toBeFalsy();
  }); 
  it('should add project', () => {
    page.navigateTo();
    let currentProjectCountPromise = page.getProjectsCount();
    page.getAddProjectLink().click();
    page.getAddProjectInput().sendKeys('Protactor test');
    page.getAddProjectInput().sendKeys( Key.ENTER );
    let newProjectCountPromise = page.getProjectsCount();
    promise.all([currentProjectCountPromise, newProjectCountPromise]).then(values => {
        expect(values[1]).toBe(values[0] + 1);
    });
  });
});
