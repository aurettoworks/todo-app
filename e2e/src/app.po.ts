import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getNavbarText() {
    return element(by.css('.navbar-brand')).getText();
  }
  getAddProjectLink() {
      return element(by.cssContainingText('.list-group-item', 'Add new project'));
  }
  getAddProjectInput() {
      return element(by.css('.col-sm-4 .list-group-item input'));
  }
  getProjectsCount() {
      return element.all(by.css('.col-sm-4 span.badge')).count();
  }
}
